#ifndef MYROBOT_H
#define MYROBOT_H

#include <QObject>
#include <QTcpSocket>
#include <QAbstractSocket>
#include <QDebug>
#include <QTimer>
#include <QMutex>


class MyRobot : public QObject {
    Q_OBJECT
public:
    explicit MyRobot(QObject *parent = nullptr);
    void doConnect();
    void disconnect();
    QByteArray DataToSend;
    QByteArray DataReceived;
    QMutex Mutex;


signals:
    void updateUI(const QByteArray Data);
public slots:
    bool isConnected();
    void connected();
    void disconnected();
    void bytesWritten(qint64 bytes);
    void readyRead();
    void MyTimerSlot();
    void MoveForward();
    void Stop();
    void MoveBackward();
    void MoveLeft();
    void MoveRight();


private:
    QTcpSocket *socket;
    QTimer *TimerEnvoi;
    bool connectStatus;
    short Crc16(QByteArray *bytes_tab, unsigned char Taille_max );
};

#endif // MYROBOT_H

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "myrobot.h"
#include <QMainWindow>
#include <QKeyEvent>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    virtual void keyPressEvent(QKeyEvent *event);
    virtual void keyReleaseEvent(QKeyEvent* event);

private slots:
    void on_connectButton_clicked();

    void on_forwardButton_pressed();

    void on_forwardButton_released();

    void on_backwardButton_pressed();

    void on_backwardButton_released();

    void on_leftButton_pressed();

    void on_leftButton_released();

    void on_rightButton_pressed();

    void on_rightButton_released();

private:
    Ui::MainWindow *ui;
    MyRobot *Robot;
};

#endif // MAINWINDOW_H

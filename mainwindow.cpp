#include "myrobot.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QWebEngineView>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
    {
        ui->setupUi(this);
        Robot = new MyRobot();
        QWebEngineView *camera = new QWebEngineView(this);
        camera->load QUrl("http://192.168.1.11:8080/?action=stream");
        camera->setContentsMargins(150,60,0,0);
        camera->resize(375, 250);
        camera->move(100,70);
        camera->show();
    }

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_connectButton_clicked()
{
   if(Robot->isConnected()){
       Robot->disconnect();
       ui->connectButton->setText("Connexion");
   }
   else{
       Robot->doConnect();
       if(Robot->isConnected())
            ui->connectButton->setText("Déconnexion");
   }
}


void MainWindow::on_forwardButton_pressed()
{
    Robot->MoveForward();
}

void MainWindow::on_forwardButton_released()
{
   Robot->Stop();
}


void MainWindow::on_backwardButton_pressed()
{
    Robot->MoveBackward();
}

void MainWindow::on_backwardButton_released()
{
    Robot->Stop();
}

void MainWindow::on_leftButton_pressed()
{
    Robot->MoveLeft();
}

void MainWindow::on_leftButton_released()
{
    Robot->Stop();
}

void MainWindow::on_rightButton_pressed()
{
    Robot->MoveRight();
}

void MainWindow::on_rightButton_released()
{
    Robot->Stop();
}

void MainWindow::keyPressEvent(QKeyEvent* event)
{
    switch (event->key()) {
        case Qt::Key_Left:
            Robot->MoveLeft();
            break;
        case Qt::Key_Right:
            Robot->MoveRight();
            break;
        case Qt::Key_Down:
            Robot->MoveBackward();
            break;
        case Qt::Key_Up:
            Robot->MoveForward();
            break;
    }
}

void MainWindow::keyReleaseEvent(QKeyEvent* event)
{
    switch (event->key()) {
        case Qt::Key_Left:
            Robot->Stop();
            break;
        case Qt::Key_Right:
            Robot->Stop();
            break;
        case Qt::Key_Down:
            Robot->Stop();
            break;
        case Qt::Key_Up:
            Robot->Stop();
            break;
    }
}
